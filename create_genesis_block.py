from hashlib import sha3_256

message = "19 Feb 2022 Putin launches huge nuclear missile exercises and sends a MIG armed with hypersonic missile over Mediterranean as US warns his 160000 troops on Ukraine border are uncoiling and poised to strike Daily Mail"

nonce = 10000
difficulty = 1
cryptohash = ""

while not cryptohash.startswith('426' * difficulty):
    nonce += 1
    cryptohash = sha3_256(message.encode('utf8') + str(nonce).encode('utf8')).hexdigest()
    print(cryptohash)

print("Done! Hash is " + cryptohash + " with nonce " + str(nonce))