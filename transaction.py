import json
import time
from hashlib import sha3_256
import base58

def transaction(fromaddrPrivate, toaddr, amount):
    if not len(fromaddrPrivate) == 93:
        print('Not a valid private key!')
        return False
    
    if not len(toaddr) == 181:
        print('Not a valid wallet address!')
        return False
    
    try:
        amount = int(amount)
    except ValueError:
        print('Not an integer')
        return False
        
    fromaddrPrivate = fromaddrPrivate[2:93-1]
    fromaddrPri16 = str(base58.b58decode(fromaddrPrivate.encode('utf8')))
    fromaddrPri16 = fromaddrPri16[2:]
    l = len(fromaddrPri16)
    fromaddrPri16 = fromaddrPri16[:l-1]
    # print(fromaddrPri16)
    fromaddr = 
    # txHashInput = json.dumps({"from": fromaddr, "to": toaddr, "time": time.time(), "amount": amount})
    # print(txHashInput)
    # txHash = sha3_256(txHashInput.encode('utf8')).hexdigest()
    # print(txHash)

fromaddrPrivate = input("What is your private key? ")
toaddr = input("Where are you sending money to? ")
amount = input("How much? ")
transaction(fromaddrPrivate, toaddr, amount)